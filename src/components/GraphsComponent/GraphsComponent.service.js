import moment from 'moment';
import HttpService from '../../_services/http.service';
import UtilityService from '../../_services/utility.service';
import {APP_SETTINGS} from '../../app.settings';

const MODULE_NAME = 'GraphsComponentService';

export default {
  name: MODULE_NAME,

  /**
   * Retrieve POC alarm list for a specified poc.
   * @param pocSerial {object}
   * @return {*|Promise.<T>}
   */
  retrieveDailyAggregates(pocSerial, numberOfUnits, levelOfAggregation) {
    this._log(`retrieveDailyAggregates, pocSerial is:${JSON.stringify(pocSerial)}`);
    const noAgrregationLevelException = {
      name: 'noAgrregationLevelException',
      message: 'You have to specifiy level of aggregation as an parametr of this function'
    };
    let unitType;
    switch (levelOfAggregation) {
      case 'Hourly':
        unitType = 'hours';
        break;
      case 'Daily':
        unitType = 'days';
        break;
      case 'Monthly':
        unitType = 'months';
        break;
      case 'Yearly':
        unitType = 'years';
        break;
      default:
        throw noAgrregationLevelException;
    }
    return HttpService.post(`${APP_SETTINGS.API_ROOT_POC}/${APP_SETTINGS.API_ENDPOINTS.DAILY_AGGREGATES}`,
      {
        fromDate: moment().subtract(numberOfUnits, unitType),
        toDate: moment(),
        level: levelOfAggregation,
        pocSerial
      })
      .then(
        response => {
          this._log('retrieveDailyAggregates completed');
          return Promise.resolve(response);
        }
      )
      .catch(
        err => {
          this._log('unable to retrieve daily aggregates data');
          return Promise.reject(err);
        }
      );
  },
  _log(message) {
    UtilityService.logging(MODULE_NAME, message);
  }
};

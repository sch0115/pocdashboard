import Vue from 'vue'
import Vuex from 'vuex'
import {APP_SETTINGS} from "./app.settings";

const pocAssignmentsMock = require('./dataMock/pocAssignments.json')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pocAssignments: []
  },
  getters: {
    onboardingSnapshot: state => date => {
      const statusSnapshot = getSnapshotTeplate()
      state.pocAssignments.map(x => {
        const assignmentStatus = getStateByDate(x.pocAssignmentEventLog, date)
        if (assignmentStatus) {
          statusSnapshot[assignmentStatus]++
          statusSnapshot.TotalAssignments ++
        }
      })
      return statusSnapshot
    },
    currentOnboardingSnapshot: state => {
      const statusSnapshot = getSnapshotTeplate()
      state.pocAssignments.map(x => {
        const assignmentStatus = x.pocAssignmentEventLog[x.pocAssignmentEventLog.length - 1].pocAssignmentEvent
        if (assignmentStatus) {
          statusSnapshot[assignmentStatus]++
          statusSnapshot.TotalAssignments ++
        }
      })
      return statusSnapshot
    }
  },
  mutations: {
    SET_POC_ASSIGNMENTS (state, pocAssignments) {
      state.pocAssignments = pocAssignments
    }
  },
  actions: {
    loadPocAssignments ({commit}) {
      commit('SET_POC_ASSIGNMENTS', pocAssignmentsMock)
    }
  }
})

function getSnapshotTeplate () {
  return {
    PocWasAssigned: 0,
    InviteWasClicked: 0,
    RegistrationFormWasSubmited: 0,
    LoginDidFail: 0,
    LoginDidSucceed: 0,
    TermsWereAccepted: 0,
    PairingStarted: 0,
    CloudCodeWasCorrect: 0,
    SasTokenWasRequested: 0,
    AssignmentEnded: 0,
    PocNotAssigned: 0,
    TotalAssignments: 0
  }
}

function getStateByDate (pocAssignmentEventLog, date) {
  // TODO test the sorting
  let pocAssignmentState = null
  for (const event of pocAssignmentEventLog) {
    if (event.timestamp < date) {
      pocAssignmentState = event.pocAssignmentEvent
    }
  }
  return pocAssignmentState
}

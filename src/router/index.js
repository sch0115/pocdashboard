import Vue from 'vue'
import Router from 'vue-router'
import Onboarding from '@/pages/onboarding/Onboarding'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Onboardnig',
      component: Onboarding
    }
  ]
})

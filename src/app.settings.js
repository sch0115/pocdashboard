export const APP_SETTINGS = {

  ENVIRONMENT_ENUM: {
    DEVELOPMENT: 'development',
    STEVE: 'steve',
    TESSA: 'tessa',
    SAMPLES: 'samples',
    PRODUCTION: 'production'
  },

  API_ENDPOINTS: {
    WEB_LOGIN: 'poc/web/login'
  }
}

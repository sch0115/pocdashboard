const MODULE_NAME = 'CosmosDBService'
// CONFIG
const config = {}
config.endpoint = '~your Azure Cosmos DB account endpoint uri here~'
config.primaryKey = '~your primary key here~'
config.database = {
  'id': 'platform'
}
config.container = {
  'id': 'Everything'
}

const endpoint = config.endpoint
const masterKey = config.primaryKey
const databaseId = config.database.id
const containerId = config.container.id

const CosmosClient = require('@azure/cosmos').CosmosClient
const client = new CosmosClient({ endpoint: endpoint, auth: { masterKey: masterKey } })

export default {
  name: MODULE_NAME,

  /**
   * Query the container using SQL
   */
  async queryContainer () {
    console.log(`Querying container:\n${config.container.id}`)

    // query to return all children in a family
    const querySpec = {
      query: 'SELECT VALUE c.pocSerial FROM c WHERE c.entityType = @entityType',
      parameters: [
        {
          name: '@entityType',
          value: 'PocAssignment'
        }
      ]
    }

    const {result: results} = await client.database(databaseId).container(containerId).items.query(querySpec).toArray()
    for (var queryResult of results) {
      let resultString = JSON.stringify(queryResult)
      console.log(`\tQuery returned ${resultString}\n`)
    }
  }
}

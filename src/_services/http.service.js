import Vue from 'vue'
import VueResource from 'vue-resource'

// import {APP_SETTINGS} from '../app.settings'

const MODULE_NAME = 'HttpService'

Vue.use(VueResource)

// Vue.http.options.root = APP_SETTINGS.API_ROOT_SEC
// Vue.http.interceptors.push((request) => {
//   const token =
//     localStorage.getItem('token') || sessionStorage.getItem('token')
//   if (request.body && request.body.email && request.body.email.toLowerCase) {
//     request.body.email = request.body.email.toLowerCase()
//   }
//
//   if (token !== null && !request.isExternal) {
//     request.headers.set('Authorization', token)
//   }
// })

export default {
  name: MODULE_NAME,

  get (url, options = {}) {
    return Vue.http.get(url, options)
  },
  getJson (url) {
    return Vue.getJson(url)
  },
  getExternal (url, options = {isExternal: true}) {
    return Vue.http.get(url, options)
  },
  post (url, body, options = {}) {
    return Vue.http.post(url, body, options)
  }
}
